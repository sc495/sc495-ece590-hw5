//
//  StudentTableViewController.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit
import os.log

class StudentTableViewController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController:nil)
    var filteredName = [Student]()
    
    func filterContentForSearchText(searchText: String, scope: String){
        filteredName = StudentList.filter{
            TempStudents in
            //let typeMatch = (scope == "MS") || (TempStudents.DegreeWorkOn == scope)
            
            //let typeMatch = (scope == "Degree") || (TempStudents.DegreeWorkOn == scope)
            if scope == "Name Search"{
                return TempStudents.FirstName.lowercased().contains(searchText.lowercased()) || TempStudents.LastName.lowercased().contains(searchText.lowercased())
            }
            return TempStudents.DegreeWorkOn.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
        
    }
    
    
    var StudentList=[Student]()
    
    func loadInitialData() {
        let student1 = Student(FirstName:"Siyang", LastName:"Chen", FromWhere:"China", Gender: "Male", Hobby: "Swimming", DegreeWorkOn: "MS", ComputerLanguage: "C++",Photo:#imageLiteral(resourceName: "Student1"),Team:"SugarFree")
        let student2 = Student(FirstName:"Nian", LastName:"Liu", FromWhere:"China", Gender: "Female", Hobby: "Sleeping", DegreeWorkOn: "Undergrad", ComputerLanguage: "Java",Photo:#imageLiteral(resourceName: "Student2"),Team:"SugarFree")
        let student3 = Student(FirstName:"Chiheng", LastName:"Liu", FromWhere:"China", Gender: "Male", Hobby: "Basketball", DegreeWorkOn: "MS", ComputerLanguage: "Swift",Photo:#imageLiteral(resourceName: "Student3"),Team:"SugarFree")
        StudentList.append(student1!)
        StudentList.append(student2!)
        StudentList.append(student3!)
        
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        navigationItem.leftBarButtonItem = editButtonItem
        if let savedStudents = loadStudents() {
            StudentList += savedStudents
        }
        else{
        loadInitialData()
        }
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        searchController.searchBar.scopeButtonTitles = ["Name Search","Degree Search"]
        searchController.searchBar.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section==0{
            return 1
        }
        if searchController.isActive && searchController.searchBar.text != ""{
            return filteredName.count
        }
        return StudentList.count   //students.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section==0{
            let cell=tableView.dequeueReusableCell(withIdentifier: "TableViewCell2",for:indexPath) as? TableViewCell2
            
            cell?.TeamName.text="SugarFree"
            return cell!
        }
        
        let cellIdentifier = "StudentTableViewCell"
        //let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let cell=tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for:indexPath) as? StudentTableViewCell
        
        
        let TempStudents:Student
        if searchController.isActive && searchController.searchBar.text != ""{
            TempStudents = filteredName[indexPath.row]
        }
        else{
            TempStudents=StudentList[indexPath.row]
        }
        cell?.Name.text=TempStudents.FirstName+" "+TempStudents.LastName
        cell?.Degree.text=TempStudents.DegreeWorkOn
        cell?.Photo.image=TempStudents.Photo
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return 30
        }
        return 100
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            if indexPath.section != 0{
                StudentList.remove(at: indexPath.row)
                saveStudents()
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        switch(segue.identifier ?? ""){
            
        case "AddItem":
            os_log("Adding a new student.", log: OSLog.default, type: .debug)
            
        case "DetailShown":
            let DestViewController:TutorialPageViewController=segue.destination as! TutorialPageViewController
            let selectedStudentCell = sender as? StudentTableViewCell
            let indexPath = tableView.indexPath(for: selectedStudentCell!)
            
            let selectedStudent:Student
            
            if searchController.isActive && searchController.searchBar.text != ""{
                selectedStudent = filteredName[(indexPath?.row)!]
            }
            else{
                selectedStudent = StudentList[(indexPath?.row)!]
            }

            DestViewController.newstudent = selectedStudent
            
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
        
        
    }
    
    
    //MARK:Unwind
    @IBAction func unwindToStudentList(segue: UIStoryboardSegue) {
        let source:StudentViewController=segue.source as! StudentViewController
        
        let newstudent:Student=source.newstudent!
        
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            // Update an existing meal.
            
            
            if searchController.isActive && searchController.searchBar.text != ""{
                filteredName[selectedIndexPath.row] = newstudent
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else{
                StudentList[selectedIndexPath.row] = newstudent
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }

        }
        else{
            
            
            if(newstudent.FirstName != ""){
                self.StudentList.append(newstudent)
            }
            self.tableView.reloadData()
            
        }
        saveStudents()
        
    }
    
    
    
    private func saveStudents(){
        let success = NSKeyedArchiver.archiveRootObject(StudentList, toFile: Student.ArchiveURL.path)
        if success{
            print("Students saved successfully")
        }
        else{
            print("Error: saving got wrong")
        }
        
    }
    
    private func loadStudents() -> [Student]?{
        return (NSKeyedUnarchiver.unarchiveObject(withFile: Student.ArchiveURL.path) as? [Student])
    }
    
    
    
}

extension StudentTableViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchText: searchController.searchBar.text!, scope: scope)
    }
    
}


extension StudentTableViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int){
        filterContentForSearchText(searchText: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
}

