//
//  KongfuScene.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit




class KongfuScene: SKScene {
    
    
    var Kongfuframes:[SKTexture]?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize){
        super.init(size:size)
        self.backgroundColor = UIColor.white
        var frames:[SKTexture] = []
        
        let beeAtlas = SKTextureAtlas(named: "Kongfu")
        
        for index in 1 ... 8{
            let textureName = "Kongfu_\(index)"
            let texture = beeAtlas.textureNamed(textureName)
            frames.append(texture)
            
        }
        self.Kongfuframes = frames
        
        
    }
    
    override func didMove(to view: SKView) {
        if(control_blur_times == true){
            
            let blur = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
            blur.alpha = 0.7
            blur.frame=self.frame
            blur.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            let vib = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blur.effect as! UIBlurEffect))
            let lab = UILabel()
            lab.text = "Kongfu World!"
            lab.sizeToFit()
            vib.frame = lab.frame
            vib.contentView.addSubview(lab)
            vib.center = CGPoint(x: blur.bounds.midX, y: blur.bounds.midY)
            vib.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
            blur.contentView.addSubview(vib)
            self.view?.addSubview(blur)
            
            control_blur_times = false
            
            //let background = SKSpriteNode(blur)
            //background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            //addChild(background)
        }
    }
    
    
    
    func KongfuShow(){
        
        /*
         let background = SKSpriteNode(imageNamed: "Background01")
         background.size = CGSize(width: self.frame.width, height:self.frame.height)
         background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
         addChild(background)
         */
        
        
        
        
        let texture = self.Kongfuframes![0]
        let BruceLee = SKSpriteNode(texture: texture)
        
        BruceLee.size = CGSize(width: 80, height: 80)
        
        let randomLeeYPositionGenerator = GKRandomDistribution(lowestValue: 50, highestValue: Int(self.frame.size.height))
        let yPosition = CGFloat(randomLeeYPositionGenerator.nextInt())
        
        
        let rightToLeft = arc4random() % 2 == 0
        
        //let yPosition = CGFloat(100)
        let xPosition = rightToLeft ? self.frame.size.width + BruceLee.size.width / 2 : -BruceLee.size.width / 2
        
        BruceLee.position = CGPoint(x: xPosition, y: yPosition)
        
        if rightToLeft {
            BruceLee.xScale = -1
        }
        
        self.addChild(BruceLee)
        
        BruceLee.run(SKAction.repeatForever(SKAction.animate(with: self.Kongfuframes!, timePerFrame: 0.2, resize: false, restore: true)))
        
        var distanceToCover = self.frame.size.width + BruceLee.size.width
        
        if rightToLeft {
            distanceToCover *= -1
        }
        
        let time = TimeInterval(abs(distanceToCover / 140))
        
        let moveAction = SKAction.moveBy(x: distanceToCover, y: 0, duration: time)
        
        let removeAction = SKAction.run {
            BruceLee.removeAllActions()
            BruceLee.removeFromParent()
        }
        
        let allActions = SKAction.sequence([moveAction, removeAction])
        
        BruceLee.run(allActions)
        
        
    }
    
    
    
    
}
