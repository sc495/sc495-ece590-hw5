//
//  Students.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//






import UIKit

class Student: NSObject, NSCoding{
    var FirstName:String
    var LastName:String
    var Team:String
    var FromWhere:String
    var Gender:String    //Male or Female
    var Hobby:String
    var Photo:UIImage
    var DegreeWorkOn:String
    var ComputerLanguage:String
    
    //Paths of Archiving
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("students")
    
    
    
    init?(FirstName:String,LastName:String,FromWhere:String,Gender:String,Hobby:String,DegreeWorkOn:String,ComputerLanguage:String,Photo:UIImage,Team:String){
        
        self.FirstName=FirstName
        self.LastName=LastName
        self.Team=Team
        self.FromWhere=FromWhere
        self.Gender=Gender
        self.Hobby=Hobby
        self.Photo=Photo
        self.DegreeWorkOn=DegreeWorkOn
        self.ComputerLanguage=ComputerLanguage
    }
    
    
    override var description: String{
        var sentences=""
        if(self.Gender=="Female"){
            sentences+="\(self.FirstName) \(self.LastName), \(self.Gender) is from \(self.FromWhere). Her degree is \(self.DegreeWorkOn) and she enjoys \(self.Hobby) outside of class. Oh, forget to mention, her proficient computer language is \(self.ComputerLanguage)."
        }
        else{
            sentences+="\(self.FirstName) \(self.LastName), \(self.Gender) is from \(self.FromWhere). His degree is \(self.DegreeWorkOn) and he enjoys \(self.Hobby) outside of class. Oh, forget to mention, his proficient computer language is \(self.ComputerLanguage)."
        }
        return sentences
    }
    
    //MARK: Types
    struct PropertyKey{
        
        static let firstname = "firstname"
        static let lastname = "lastname"
        static let team = "team"
        static let location = "location"
        static let gender = "gender"
        static let hobbies = "hobbies"
        static let photo = "photo"
        static let degree = "degree"
        static let computerlanguage = "computerlanguage"
        
        
    }
    
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(FirstName, forKey: PropertyKey.firstname)
        aCoder.encode(LastName, forKey: PropertyKey.lastname)
        aCoder.encode(Team, forKey: PropertyKey.team)
        aCoder.encode(FromWhere, forKey: PropertyKey.location)
        aCoder.encode(Gender, forKey: PropertyKey.gender)
        aCoder.encode(Hobby, forKey: PropertyKey.hobbies)
        aCoder.encode(Photo, forKey: PropertyKey.photo)
        aCoder.encode(DegreeWorkOn, forKey: PropertyKey.degree)
        aCoder.encode(ComputerLanguage, forKey: PropertyKey.computerlanguage)
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let firstname = aDecoder.decodeObject(forKey: PropertyKey.firstname) as? String
        let lastname = aDecoder.decodeObject(forKey: PropertyKey.lastname) as? String
        let team = aDecoder.decodeObject(forKey: PropertyKey.team) as? String
        let location = aDecoder.decodeObject(forKey: PropertyKey.location) as? String
        let gender = aDecoder.decodeObject(forKey: PropertyKey.gender) as? String
        let hobbies = aDecoder.decodeObject(forKey: PropertyKey.hobbies) as? String
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        let degree = aDecoder.decodeObject(forKey: PropertyKey.degree) as? String
        let computerlanguage = aDecoder.decodeObject(forKey: PropertyKey.computerlanguage) as? String
        
        self.init(FirstName:firstname!,LastName:lastname!,FromWhere:location!,Gender:gender!,Hobby:hobbies!,DegreeWorkOn:degree!,ComputerLanguage:computerlanguage!,Photo:photo!,Team:team!)
        
        
    }
    
    
}


