//
//  GameScene.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit


class GameScene: SKScene {
    
    var Circle = SKSpriteNode()
    var Person = SKSpriteNode()
    
    var Path = UIBezierPath()
    
    var gameStarted = Bool()
    
    
    override func didMove(to view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "Background01")
        background.size = CGSize(width: self.frame.width, height:self.frame.height)
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        addChild(background)
        
        
        
        Circle = SKSpriteNode(imageNamed:"Circle")
        Circle.color=UIColor.red
        Circle.size = CGSize(width: 150, height: 150)
        Circle.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        self.addChild(Circle)
        
        Person = SKSpriteNode(imageNamed:"Basketball")
        Person.size = CGSize(width: 40, height: 40)
        Person.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 + 40)
        self.addChild(Person)
        
        
        //Circle
        moveClockWise()
        
    }
    
    
    
    func moveClockWise(){
        
        let dx = Person.position.x - self.frame.width / 2
        let dy = Person.position.y - self.frame.height / 2
        
        let rad = atan2(dy, dx)
        
        Path = UIBezierPath(arcCenter: CGPoint(x: self.frame.width / 2, y: self.frame.width / 2), radius: 40, startAngle: rad, endAngle: rad+CGFloat(M_PI*2), clockwise: true)
        
        let follow = SKAction.follow(Path.cgPath, asOffset: false, orientToPath: true, speed: 200)
        Person.run(SKAction.repeatForever(follow).reversed())
        
    }
    
    func moveCounterClockWise(){
        
    }
    
    
    
}
