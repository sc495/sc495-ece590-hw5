//
//  V2ViewController.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit

class V2ViewController: UIViewController {
    
    
    
    var Studentinfo:Student?
    
    @IBOutlet weak var Show: UILabel!
    var ShowText:String=""
    
    @IBOutlet weak var ImagePho: UIImageView!
    var PhotoImg=UIImage(named: "Default")
    
    override func viewDidLoad() {
        //super.viewDidLoad()
        //Show.text=ShowText
        //ImagePho.image=PhotoImg
        Show.text=Studentinfo?.description
        ImagePho.image=Studentinfo?.Photo
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier=="DetailChange"){
            
            // Get the new view controller using segue.destinationViewController.
            // Pass the selected object to the new view controller.
            let DestViewController:StudentViewController=segue.destination as! StudentViewController
            let selectedStudent=Studentinfo
            DestViewController.newstudent=selectedStudent
            
            /*
             DestViewController.FirstName.text=Studentinfo?.FirstName
             DestViewController.LastName.text=Studentinfo?.LastName
             DestViewController.FromWhere.text=Studentinfo?.FromWhere
             DestViewController.Gender.text=Studentinfo?.Gender
             DestViewController.Hobby.text=Studentinfo?.Hobby
             DestViewController.DegreeWorkOn.text=Studentinfo?.DegreeWorkOn
             DestViewController.ComputerLanguage.text=Studentinfo?.ComputerLanguage
             DestViewController.Photo.image=Studentinfo?.Photo
             DestViewController.Output.text=Studentinfo?.description
             */
            
        }
    }
    
    
}

