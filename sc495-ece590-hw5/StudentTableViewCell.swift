//
//  StudentTableViewCell.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {
    
    
    //MARK:Attributes
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Degree: UILabel!
    @IBOutlet weak var Photo: UIImageView!
    @IBOutlet weak var Background: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //Background.image=UIImage(named:"Background01")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
