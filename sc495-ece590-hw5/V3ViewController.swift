//
//  V3ViewController.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation


var control_blur_times = true

class V3ViewController: UIViewController {
    
    @IBOutlet weak var sceneView: SKView!
    
    @IBOutlet weak var sceneView2: SKView!
    
    var scene:KongfuScene?
    var scene2:GameScene?
    
    var MusicPlayer=AVAudioPlayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        control_blur_times = true
        // Do any additional setup after loading the view.
        
        do{
            MusicPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "sounds", ofType:"mp3")!))
            MusicPlayer.prepareToPlay()
            MusicPlayer.play()
        }
        catch{
            print("music loaded failed")
        }
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.scene = KongfuScene(size: CGSize(width: self.sceneView.frame.size.width, height: self.sceneView.frame.size.height))
        self.sceneView.presentScene(scene)
        
        self.scene2 = GameScene(size: CGSize(width: self.sceneView2.frame.size.width, height: self.sceneView2.frame.size.height))
        scene2?.scaleMode = .aspectFill
        
        self.sceneView2.presentScene(scene2)
        
        
        
        
        
    }
    
    
    
    //MARK: - Actions
    
    @IBAction func ShowAnimation(_ sender: UIButton) {
        if let scene = self.scene{
            scene.KongfuShow()
        }
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

