//
//  ViewController.swift
//  sc495-ece590-hw5
//
//  Created by Siyang Chen on 2/16/17.
//  Copyright © 2017 Siyang Chen. All rights reserved.
//

import UIKit
import os.log



class StudentViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    var newstudent:Student?
    
    var sex = "Male"
    var degree = "MS"
    
    //MARK:Attributes
    @IBOutlet weak var FirstName: UITextField!
    @IBOutlet weak var LastName: UITextField!
    @IBOutlet weak var FromWhere: UITextField!
    @IBOutlet weak var Hobby: UITextField!
    @IBOutlet weak var ComputerLanguage: UITextField!
    @IBOutlet weak var SexSelectDP: UISegmentedControl!
    @IBOutlet weak var pickerview: UIPickerView!
    
    let degrees = ["Undergrad", "MS", "Phd", "Post Doc", "Others"]
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return degrees[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return degrees.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        degree = degrees[row]
    }
    
    
    
    
    
    
    
    //TextView For Description
    @IBOutlet weak var Output: UILabel!
    //PhotoTaken
    @IBOutlet weak var Photo: UIImageView!
    //SaveButton
    @IBOutlet weak var SaveButton: UIBarButtonItem!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        FirstName.delegate=self
        LastName.delegate=self
        FromWhere.delegate=self
        Hobby.delegate=self
        ComputerLanguage.delegate=self
        pickerview.delegate=self
        if((newstudent) != nil){
            print("we passed the data properly!")
            FirstName.text = newstudent?.FirstName
            LastName.text=newstudent?.LastName
            FromWhere.text=newstudent?.FromWhere
            if(newstudent?.Gender=="Female"){
                SexSelectDP.selectedSegmentIndex = 1
            }
            else{
                SexSelectDP.selectedSegmentIndex = 0
            }
            
            //Gender.text=newstudent?.Gender
            
            Hobby.text=newstudent?.Hobby
            
            //let degrees = ["Undergraduate", "MS", "Phd", "Post Doc", "Others"]
            if(newstudent?.DegreeWorkOn == "Undergraduate"){
                    pickerview.selectRow(0, inComponent: 0, animated: true)
            }
            else if(newstudent?.DegreeWorkOn == "MS"){
                pickerview.selectRow(1, inComponent: 0, animated: true)
            }
            else if(newstudent?.DegreeWorkOn == "Phd"){
                pickerview.selectRow(2, inComponent: 0, animated: true)
            }
            else if(newstudent?.DegreeWorkOn == "Post Doc"){
                pickerview.selectRow(3, inComponent: 0, animated: true)
            }
            else{
                pickerview.selectRow(4, inComponent: 0, animated: true)
            }
            //DegreeWorkOn.text=newstudent?.DegreeWorkOn
            
            
            ComputerLanguage.text=newstudent?.ComputerLanguage
            Output.text=newstudent?.description
            Photo.image=newstudent?.Photo
            
        }
    }
    
    
    
    //MARK:UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Hide keyboard
        FirstName.resignFirstResponder()
        LastName.resignFirstResponder()
        FromWhere.resignFirstResponder()
        //Gender.resignFirstResponder()
        Hobby.resignFirstResponder()
        ComputerLanguage.resignFirstResponder()
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        Photo.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK:Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((sender as! UIBarButtonItem) != self.SaveButton)  {
            return
        }
        
        let FN=FirstName.text
        let LN=LastName.text
        let FW=FromWhere.text
        let SEX=sex
        let HB=Hobby.text
        let DEG=degree
        let COML=ComputerLanguage.text
        let TM="SugarFree"
        
        newstudent=Student(FirstName: FN!, LastName: LN!, FromWhere: FW!, Gender: SEX, Hobby: HB!, DegreeWorkOn: DEG, ComputerLanguage: COML!,Photo:Photo.image!,Team:TM)
        
        
    }
    
    
    
    @IBAction func Cancel(_ sender: UIBarButtonItem) {
        let isPresentingInAddStudentMode = presentingViewController is UINavigationController
        if isPresentingInAddStudentMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        }
        
        
    }
    
    
    
    
    @IBAction func SexSelect(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            sex="Male"
        }
        else{
            sex="Female"
        }
    }
    
    
    
    
    //MARK:Actions
    
    @IBAction func Takephoto(_ sender: UITapGestureRecognizer) {
        let imagePick = UIImagePickerController()
        imagePick.sourceType = .camera
        imagePick.delegate = self
        present(imagePick, animated: true, completion: nil)
    }
    
    @IBAction func Register(_ sender: UIButton) {
        print("Default Text Button Tapped")
        
        
        let FN=FirstName.text
        let LN=LastName.text
        let FW=FromWhere.text
        let SEX=sex
        let HB=Hobby.text
        let DEG=degree
        let COML=ComputerLanguage.text
        let TM="SugarFree"
        
        let classbuilt = Student(FirstName: FN!, LastName: LN!, FromWhere: FW!, Gender: SEX, Hobby: HB!, DegreeWorkOn: DEG, ComputerLanguage: COML!,Photo:Photo.image!,Team:TM)
        
        
        Output.text=classbuilt?.description
        
    }
    
    
    @IBAction func KillKeyboard(_ sender: UITapGestureRecognizer) {
        FirstName.resignFirstResponder()
        LastName.resignFirstResponder()
        FromWhere.resignFirstResponder()
        //Gender.resignFirstResponder()
        Hobby.resignFirstResponder()
        ComputerLanguage.resignFirstResponder()
        
    }
    
    
    
    
    
    /*Good staff, please keep
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     let DestViewController:V2ViewController=segue.destination as! V2ViewController
     DestViewController.ShowText=Output.text!
     DestViewController.PhotoImg=Photo.image
     
     
     */
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


